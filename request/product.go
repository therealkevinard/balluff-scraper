package request

//--- Products
type Product struct {
	Key          string            `json:"key"`
	ParentKey    string            `json:"parent_key"`
	ChildKey     string            `json:"child_key"`
	Label        string            `json:"label"`
	Count        int               `json:"count"`
	Columns      map[string]string `json:"columns"`
	BulletPoints []string          `json:"bulletpoints"`
	Thumbnail    string            `json:"image_url"`
	Image        string            `json:"full_size_image_url"`
}

type Products struct {
	Type  string    `json:"type"`
	Count int       `json:"count"`
	Items []Product `json:"items"`
}

type ProductsResponse struct {
	Page Products `json:"page"`
}
