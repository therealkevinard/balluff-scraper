package request

//--- Categories
type Category struct {
	Key       string `json:"key"`
	ParentKey string `json:"parent_key"`
	Label     string `json:"label"`
	Count     int    `json:"count"`
}

type Categories struct {
	Type    string     `json:"type"`
	Count   int        `json:"count"`
	Columns []string   `json:"columns"`
	Items   []Category `json:"items"`
}

type CategoriesResponse struct {
	Page Categories `json:"page"`
}
