package request

//--- Variants
type ProductVariant struct {
	LocalId       int               `json:"local_id"`
	Category      string            `json:"category"`
	ChildCategory string            `json:"child_category"`
	Product       string            `json:"parent_product"`
	Key           string            `json:"key"`
	Label         string            `json:"label"`
	Thumbnail     string            `json:"thumbnail_url"`
	Columns       map[string]string `json:"columns"`
}

type ProductVariants struct {
	Items []ProductVariant `json:"items"`
}

type ProductVariantsResponse struct {
	Page ProductVariants `json:"page"`
}
