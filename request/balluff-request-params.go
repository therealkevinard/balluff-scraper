package request

import "github.com/gin-gonic/gin/json"

//--- API Call Params
type Selection struct {
	Ca      string `json:"ca"`
	Cg      string `json:"cg"`
	Product string `json:"product"`
	Variant string `json:"product_variant"`
}

type RequestParams struct {
	Selection Selection `json:"selection"`
	Locale    string    `json:"locale"`
}

// Marshals self into a json payload for rest calls
func (b *RequestParams) Payload() ([]byte, error) {
	return json.Marshal(b)
}

// Setting locale is redundant.
func (b *RequestParams) SetLocale(string) {
	// @todo: complete
}
