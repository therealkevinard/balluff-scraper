package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/therealkevinard/balluff-scraper/request"
	"io/ioutil"
	"net/http"
	"os"
	"runtime"
	"time"
)

var domain = "https://search.balluff.com/"
var baseUrl = "api/"
var logfile = "/home/kevin/workspaces/go/src/gitlab.com/therealkevinard/balluff-scraper/products.log" // @todo: there's a better way.

// wrapper for making rest call with provided params
// handles the heavy-lifting for making the actual request and reading the response into []byte
func makeRequest(prm request.RequestParams) ([]byte, error) {
	reqBytes, err := prm.Payload()
	req, err := http.NewRequest(http.MethodPost, domain+baseUrl, bytes.NewBuffer(reqBytes))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	body, _ := ioutil.ReadAll(res.Body)

	defer res.Body.Close()
	return body, err
}

// @todo: the load* methods are on fire with CPD. refactor with a higher-level interface/generator.

// Loads top-level categories
func loadCategories() ([]request.Category, error) {
	catsResponse := request.CategoriesResponse{}
	prm := request.RequestParams{
		Locale: "en-US",
	}

	body, err := makeRequest(prm)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(body, &catsResponse)
	if err != nil {
		return nil, err
	}

	return catsResponse.Page.Items, err
}

// Loads child categories for given parent
func loadChildCategories(parentKey string) ([]request.Category, error) {
	catsResponse := request.CategoriesResponse{}
	prm := request.RequestParams{
		Locale: "en-US",
		Selection: request.Selection{
			Ca: parentKey,
		},
	}

	body, err := makeRequest(prm)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(body, &catsResponse)
	if err != nil {
		return nil, err
	}

	// we _could_return catsResponse.Page.Items, but we need to prop the parent cat down for later use
	var collect []request.Category
	for _, item := range catsResponse.Page.Items {
		item.ParentKey = parentKey
		collect = append(collect, item)
	}
	return collect, err
}
func workerLoadChildCategories(
	jobs <-chan request.Category,
	results chan<- []request.Category,
) {
	for j := range jobs {
		cc, _ := loadChildCategories(j.Key)
		results <- cc
	}
}

// Loads products
func loadProducts(parentKey string, childKey string) ([]request.Product, error) {
	productsResponse := request.ProductsResponse{}
	prm := request.RequestParams{
		Locale: "en-US",
		Selection: request.Selection{
			Ca: parentKey,
			Cg: childKey,
		},
	}

	body, err := makeRequest(prm)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(body, &productsResponse)
	if err != nil {
		return nil, err
	}

	// again - we need to mutate the items to include everything necc. for later queries
	var collect []request.Product
	for _, item := range productsResponse.Page.Items {
		item.ParentKey = parentKey
		item.ChildKey = childKey
		collect = append(collect, item)
	}

	return collect, err
}
func workerLoadProducts(
	jobs <-chan request.Category,
	results chan<- []request.Product,
) {
	for j := range jobs {
		cp, _ := loadProducts(j.ParentKey, j.Key)
		results <- cp
	}
}

// Loads product variants
func loadProductVariants(parentKey string, childKey string, productKey string) ([]request.ProductVariant, error) {
	productVariants := request.ProductVariantsResponse{}
	prm := request.RequestParams{
		Locale: "en-US",
		Selection: request.Selection{
			Ca:      parentKey,
			Cg:      childKey,
			Product: productKey,
		},
	}

	body, err := makeRequest(prm)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(body, &productVariants)
	if err != nil {
		return nil, err
	}

	var collect []request.ProductVariant
	for _, item := range productVariants.Page.Items {
		item.Category = parentKey
		item.ChildCategory = childKey
		item.Product = productKey
		collect = append(collect, item)
	}
	return collect, err
}
func workerLoadProductVariants(
	jobs <-chan request.Product,
	results chan<- []request.ProductVariant) {
	for j := range jobs {
		cv, _ := loadProductVariants(j.ParentKey, j.ChildKey, j.Key)
		results <- cv
	}
}

func announce(label string, count int, since time.Duration) {
	fmt.Println(since)
	fmt.Printf("Pool Finished: %s \n\t%d entities\n", label, count)
}

func main() {
	f, err := os.OpenFile(logfile, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		panic("failed opening logfile")
	}

	defer f.Close()

	start := time.Now()
	//----------------------------------------- Parent Categories
	cats, _ := loadCategories()

	poolSize := runtime.NumCPU() - 2
	fmt.Printf("Using %d cores\n", poolSize)
	//----------------------------------------- worker pool - Child Categories
	childCatJobs := make(chan request.Category, 50)
	childCatResults := make(chan []request.Category, 50)
	var childCatCollection []request.Category
	for wid := 1; wid <= poolSize; wid++ {
		go workerLoadChildCategories(childCatJobs, childCatResults)
	}

	for _, cat := range cats {
		childCatJobs <- cat
	}
	close(childCatJobs)

	for a := 1; a <= len(cats); a++ {
		result := <-childCatResults
		for _, childcat := range result {
			childCatCollection = append(childCatCollection, childcat)
		}
	}
	close(childCatResults)
	announce("Child Categories", len(childCatCollection), time.Since(start))

	//----------------------------------------- worker pool - Products
	productJobs := make(chan request.Category, 50)
	productResults := make(chan []request.Product, 50)
	var productCollection []request.Product
	for wid := 1; wid <= poolSize; wid++ {
		go workerLoadProducts(productJobs, productResults)
	}

	for _, childcat := range childCatCollection {
		productJobs <- childcat
	}
	close(productJobs)

	for a := 1; a <= len(childCatCollection); a++ {
		result := <-productResults
		for _, product := range result {
			productCollection = append(productCollection, product)
		}
	}
	close(productResults)
	announce("Products", len(productCollection), time.Since(start))

	//----------------------------------------- worker pool - Variants
	currentVariant := 1
	variantJobs := make(chan request.Product, 5000)
	variantResults := make(chan []request.ProductVariant, 5000)
	for wid := 1; wid <= poolSize; wid++ {
		go workerLoadProductVariants(variantJobs, variantResults)
	}

	for _, product := range productCollection {
		variantJobs <- product
	}
	close(variantJobs)

	for a := 1; a <= len(productCollection); a++ {
		result := <-variantResults
		for _, variant := range result {
			variant.LocalId = currentVariant
			jsonVal, _ := json.Marshal(variant)
			_, err := f.WriteString(string(jsonVal) + "\n")
			if err != nil {
				fmt.Printf("Failed logging %d to file\n", currentVariant)
			}
			currentVariant++
		}
	}
	close(variantResults)
	announce("Variants", currentVariant, time.Since(start))
}
